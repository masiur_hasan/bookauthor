﻿using BookAuthor.Models.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.IRepository
{
    public interface IBookRepository
    {
        Books GetBook(int Id);
        IEnumerable<Books> GetAllBooks();
        Books Add(Books book);
        Books Update(Books book);
        Books Delete(int Id);
    }
}
