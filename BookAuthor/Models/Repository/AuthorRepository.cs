﻿using BookAuthor.Models.Author;
using BookAuthor.Models.Context;
using BookAuthor.Models.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.Repository
{
    public class AuthorRepository : IAuthorRepository
    {

        private readonly AppDbContext _context;

        public AuthorRepository(AppDbContext context)
        {
            _context = context;
        }

        public Authors Add(Authors author)
        {
            _context.Authors.Add(author);
            _context.SaveChanges();
            return author;
        }

        public Authors Delete(int Id)
        {
            Authors author = _context.Authors.Find(Id);
            if (author != null)
            {
                _context.Authors.Remove(author);
                _context.SaveChanges();
            }
            return author;
        }

        public IEnumerable<Authors> GetAllAuthors()
        {
            return _context.Authors;
        }

        public Authors GetAuthor(int Id)
        {
            return _context.Authors.Find(Id);
        }

        public Authors Update(Authors author)
        {
            _context.Authors.Update(author);
            _context.SaveChanges();
            return author;
        }
    }
}
