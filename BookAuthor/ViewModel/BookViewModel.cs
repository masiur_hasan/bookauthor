﻿using BookAuthor.Models.BookAuthor;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.ViewModel
{
    public class BookViewModel
    {
        public string Name { get; set; }
        public int ISBN { get; set; }
        public DateTime PublishingDate { get; set; }
        public string Adition { get; set; }
        public string Part { get; set; }
        public string Description { get; set; }
    }
}
