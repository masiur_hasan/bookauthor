﻿using BookAuthor.Models.Author;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.IService
{
    public interface IAuthorService
    {
        Authors GetAuthor(int Id);
        IEnumerable<Authors> GetAllAuthors();
        Authors Add(Authors author);
        Authors Update(Authors author);
        Authors Delete(Authors author);
    }
}
