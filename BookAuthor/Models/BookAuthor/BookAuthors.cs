﻿using BookAuthor.Models.Author;
using BookAuthor.Models.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.BookAuthor
{
    public class BookAuthors
    {
        public int BookId { get; set; }
        public Books Book { get; set; }

        public int AuthorId { get; set; }
        public Authors Author { get; set; }
    }
}
