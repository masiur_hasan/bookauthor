﻿using BookAuthor.Models.BookAuthor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.Author
{
    public class Authors
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DOB { get; set; }

        public List<BookAuthors> BookAuthors { get; set; }
    }
}
