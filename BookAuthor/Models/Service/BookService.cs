﻿using BookAuthor.Models.Book;
using BookAuthor.Models.Context;
using BookAuthor.Models.IRepository;
using BookAuthor.Models.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.Service
{
    public class BookService : IBookService
    {
        private readonly IRepository<Books> _bookRepository;
        private readonly AppDbContext _context;
        public BookService(IRepository<Books> bookRepository, AppDbContext context)
        {
            _bookRepository = bookRepository;
            _context = context;
        }
        public Books Add(Books book)
        {
            var newBook =_bookRepository.Insert(book);
            return newBook;
        }

        public Books Delete(Books book)
        {
            var newBook = _bookRepository.Delete(book);
            return newBook;
        }

        public IEnumerable<Books> GetAllBooks()
        {
            return _context.Books;
        }

        public Books GetBook(int Id)
        {
            var book = _bookRepository.GetById(Id);
            return book;
        }

        public Books Update(Books book)
        {
            var newBook = _bookRepository.Update(book);
            return newBook;
        }
    }
}
