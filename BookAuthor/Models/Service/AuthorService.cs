﻿using BookAuthor.Models.Author;
using BookAuthor.Models.Context;
using BookAuthor.Models.IRepository;
using BookAuthor.Models.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.Service
{
    public class AuthorService : IAuthorService
    {
        private readonly IRepository<Authors> _authorRepository;
        private readonly AppDbContext _context;
        public AuthorService(IRepository<Authors> authorRepository, AppDbContext context)
        {
            _authorRepository = authorRepository;
            _context = context;
        }
        public Authors Add(Authors author)
        {
            var newAuthor = _authorRepository.Insert(author);
            return newAuthor;
        }

        public Authors Delete(Authors author)
        {
            var newAuthor = _authorRepository.Delete(author);
            return newAuthor;
        }

        public IEnumerable<Authors> GetAllAuthors()
        {
            return _context.Authors;
        }

        public Authors GetAuthor(int Id)
        {
            var author = _authorRepository.GetById(Id);
            return author;
        }

        public Authors Update(Authors author)
        {
            var newAuthor = _authorRepository.Update(author);
            return newAuthor;
        }
    }
}
