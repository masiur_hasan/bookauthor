﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models
{
    public abstract partial class BaseEntity
    {
        public int Id { get; set; }
    }
}
