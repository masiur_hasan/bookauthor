﻿using BookAuthor.Models.Author;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.IRepository
{
    public interface IAuthorRepository
    {
        Authors GetAuthor(int Id);
        IEnumerable<Authors> GetAllAuthors();
        Authors Add(Authors author);
        Authors Update(Authors author);
        Authors Delete(int Id);
    }
}
