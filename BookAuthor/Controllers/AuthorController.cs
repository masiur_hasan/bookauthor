﻿using BookAuthor.Models.Author;
using BookAuthor.Models.Book;
using BookAuthor.Models.Context;
using BookAuthor.Models.IRepository;
using BookAuthor.Models.IService;
using BookAuthor.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IAuthorService _authorService;
        private readonly AppDbContext _context;


        public AuthorController(IAuthorRepository authorRepository, AppDbContext context, IAuthorService authorService)
        {
            _authorRepository = authorRepository;
            _authorService = authorService;
            _context = context;
        }

        public IActionResult Index()
        {
            var authors = _authorService.GetAllAuthors();
            return View(authors);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(AuthorViewModel author)
        {
            if (ModelState.IsValid)
            {
                var newauthor = new Authors{
                    Name = author.Name,
                    DOB = author.DOB,
                    Description = author.Description
                };
                Authors Auhor = _authorService.Add(newauthor);
                return RedirectToAction("Index","Author");
            }
            return View();
        }

        [HttpGet]
        public ViewResult Delete(int Id)
        {
            var author = _authorService.GetAuthor(Id);
            return View(author);
        }

        [HttpPost]
        public IActionResult Delete(Authors author)
        {
            var bookAuthors = _context.BookAuthors.Where(x => x.AuthorId == author.Id).ToList();
            foreach (var item in bookAuthors)
            {
                _context.BookAuthors.Remove(item);
            }
            _context.SaveChanges();
            Authors authors = _authorService.Delete(author);
            return RedirectToAction("Index", "Author");
        }

        [HttpGet]
        public ViewResult Update(int Id)
        {
            var author = _authorService.GetAuthor(Id);
            return View(author);
        }

        [HttpPost]
        public IActionResult Update(Authors author)
        {
            if (ModelState.IsValid)
            {
                Authors newAuthor = _authorService.Update(author);
                return RedirectToAction("Index", "Author");
            }
            return View();
        }

        [HttpGet]
        public ViewResult GetBook(int Id)
        {
            var author = _authorService.GetAuthor(Id);
            var bookAuthors = _context.BookAuthors.Where(x => x.AuthorId == Id).ToList();
            var books = new List<Books>();
            foreach (var item in bookAuthors)
            {
                var book = _context.Books.Where(x => x.Id == item.BookId).FirstOrDefault();
                books.Add(book);
            }
            ViewData["AuthorId"] = books;
            return View(author);
        }

    }
}
