﻿using BookAuthor.Models.Book;
using BookAuthor.Models.Context;
using BookAuthor.Models.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.Repository
{
    public class BookRepository : IBookRepository
    {

        private readonly AppDbContext _context;

        public BookRepository(AppDbContext context)
        {
            _context = context;
        }

        public Books Add(Books book)
        {
            _context.Books.Add(book);
            _context.SaveChanges();
            return book;
        }

        public Books Delete(int Id)
        {
            Books book = _context.Books.Find(Id);
            if (book != null)
            {
                _context.Books.Remove(book);
                _context.SaveChanges();
            }
            return book;
        }

        public IEnumerable<Books> GetAllBooks()
        {
            return _context.Books;
        }

        public Books GetBook(int Id)
        {
            return _context.Books.Find(Id);
        }

        public Books Update(Books book)
        {
            _context.Books.Update(book);
            _context.SaveChanges();
            return book;
        }
    }
}
