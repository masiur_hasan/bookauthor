﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.ViewModel
{
    public class AuthorViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DOB { get; set; }
    }
}
