﻿using BookAuthor.Models.BookAuthor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Models.Book
{
    public class Books
    {
            public int Id { get; set; }
            public string Name { get; set; }
            public int ISBN { get; set; }
            public DateTime PublishingDate { get; set; }
            public string Adition { get; set; }
            public string Part { get; set; }
            public string Description { get; set; }

            public List<BookAuthors> BookAuthors { get; set; }
    }
}
