﻿using BookAuthor.Models.Author;
using BookAuthor.Models.Book;
using BookAuthor.Models.BookAuthor;
using BookAuthor.Models.Context;
using BookAuthor.Models.IRepository;
using BookAuthor.Models.IService;
using BookAuthor.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAuthor.Controllers
{
    public class BookController : Controller
    {

        private readonly IBookRepository _bookRepository;
        private readonly IBookService _bookService;
        private readonly AppDbContext _context;


        public BookController(IBookRepository bookRepository, AppDbContext context, IBookService bookService)
        {
            _bookRepository = bookRepository;
            _context = context;
            _bookService = bookService;
        }

        public IActionResult Index()
        {
            var books = _bookRepository.GetAllBooks();
            return View(books);
        }

        [HttpGet]
        public ViewResult Create()
        {
            List<SelectListItem> selectListItems = _context.Authors.Select(a => new SelectListItem
            {
                Text = a.Name,
                Value = a.Id.ToString()
            }).ToList();
            ViewBag.AuthorId = selectListItems;
            return View();
        }

        [HttpPost]
        public IActionResult Create(BookViewModel book, List<int> AuthorIdList)
        {
            if (ModelState.IsValid)
            {
                var Book = new Books
                {
                    Name = book.Name,
                    ISBN = book.ISBN,
                    Adition = book.Adition,
                    Part = book.Part,
                    PublishingDate = book.PublishingDate,
                    Description = book.Description
                };
                Books newBook = _bookService.Add(Book);
                foreach (var item in AuthorIdList)
                {
                    BookAuthors bookAuthors = new BookAuthors()
                    {
                        AuthorId = item,
                        BookId = newBook.Id,
                        Author = _context.Authors.Where(x => x.Id == item).FirstOrDefault(),
                        Book = newBook
                    };
                    _context.BookAuthors.Add(bookAuthors);
                }

                _context.SaveChanges();
                
                return RedirectToAction("Index", "Book");

            }
            return View();
        }

        [HttpGet]
        public ViewResult Delete(int Id)
        {
            var book = _bookService.GetBook(Id);
            return View(book);
        }

        [HttpPost]
        public IActionResult Delete(Books book)
        {
            var bookAuthors = _context.BookAuthors.Where(x => x.BookId == book.Id).ToList();
            foreach(var item in bookAuthors)
            {
                _context.BookAuthors.Remove(item);
            }
            _context.SaveChanges();
            Books books = _bookService.Delete(book);
            return RedirectToAction("Index", "Book");
        }

        [HttpGet]
        public ViewResult Update(int Id)
        {
            var book = _bookService.GetBook(Id);
            List<SelectListItem> selectListItems = _context.Authors.Select(a => new SelectListItem
            {
                Text = a.Name,
                Value = a.Id.ToString()
            }).ToList();
            ViewBag.AuthorId = selectListItems;
            return View(book);
        }

        [HttpPost]
        public IActionResult Update(Books book, List<int> AuthorIdList)
        {
            if (ModelState.IsValid)
            {
                Books newBook = _bookService.Update(book);

                var bookAuthors = _context.BookAuthors.Where(x => x.BookId == newBook.Id).ToList();
                foreach (var item in bookAuthors)
                {
                    _context.BookAuthors.Remove(item);
                }
                _context.SaveChanges();

                foreach (var it in AuthorIdList)
                {
                    //var isExist = _context.BookAuthors.Where(x => x.AuthorId == item && x.BookId == newBook.Id);
                    //if(isExist)
                    //{
                    BookAuthors bookAuthor = new BookAuthors()
                    {
                        AuthorId = it,
                        BookId = newBook.Id,
                        Author = _context.Authors.Where(x => x.Id == it).FirstOrDefault(),
                        Book = newBook
                    };
                    _context.BookAuthors.Add(bookAuthor);
                    //}

                }
                _context.SaveChanges();

                return RedirectToAction("Index", "Book");
                
            }
            return View();
        }

        [HttpGet]
        public ViewResult GetAuthor(int Id)
        {
            var book = _bookService.GetBook(Id);
            var bookAuthors = _context.BookAuthors.Where(x => x.BookId == Id).ToList();
            var authors = new List<Authors>();
            foreach(var item in bookAuthors)
            {
                var author = _context.Authors.Where(x => x.Id == item.AuthorId).FirstOrDefault();
                authors.Add(author);
            }
            ViewData["AuthorId"] = authors;
            return View(book);
        }

    }
}
